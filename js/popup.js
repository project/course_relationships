(function ($) {
  Drupal.behaviors.course_relationships_popup = {
    attach: function (context, settings) {
      $("#course-relationships-popup").dialog({
        draggable: false,
        modal: true
      });
    }
  };
}(jQuery));
